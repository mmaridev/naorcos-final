#!/usr/bin/python3
# Copyright (c) 201* Marco Marinello <marco.marinello@school.rainerum.it>
# See the GNU General Public License version 3

from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import datetime
import html2text
import telegram
from telegram.error import NetworkError, Unauthorized
import configparser
import locale

try:
    import connector
except:
    print("This application requires a proprietary plugin")
    exit(1)


class RegisterAPI(BaseHTTPRequestHandler):
    def tgmsg(self, u, msg):
        header = "Ciao %s," % u
        closing = "Con affetto, NAO"
        self.bot.sendMessage(self.dests[u], "%s\n\n%s\n\n%s" % (header, msg, closing))


    def domani(self, u, session, compiti):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        out = "Per domani hai:"
        i = 0
        for co in compiti:
            if datetime.datetime.utcfromtimestamp(int(co['data_inserimento'])).date() == tomorrow:
                out += " "
                out += session.subjects[co["materia_id"]]
                out += ". "
                out += html2text.html2text(session.ask("get_compito", {"coid": co["com_id"]})["descrizione"]).replace("\n", " ")
                i += 1
        if i == 0:
            out = "Per domani non hai compiti! Divertiti questo pomeriggio."
        self.tgmsg(u, out)
        return out


    def settimana(self, u, session, compiti):
        today = datetime.date.today()
        sevendays = datetime.date.today() + datetime.timedelta(days=7)
        out = "Per i prossimi giorni hai:"
        last_day = ""
        i = 0
        for co in compiti:
            if today < datetime.datetime.utcfromtimestamp(int(co['data_inserimento'])).date() < sevendays:
                this_day = datetime.datetime.utcfromtimestamp(int(co['data_inserimento'])).strftime("%A %d")
                tmp = ""
                if this_day != last_day:
                    tmp += "\n" + this_day + "."
                    last_day = this_day
                else:
                    tmp += ";"
                tmp += " "
                tmp += session.subjects[co["materia_id"]]
                tmp += ". "
                tmp += html2text.html2text(session.ask("get_compito", {"coid": co["com_id"]})["descrizione"]).replace("\n", " ")
                tmp += "."
                if "inglese" not in session.subjects[co["materia_id"]].lower():
                    out += tmp
                i += 1
        if i == 0:
            out = "Per i prossimi giorni non hai compiti! Occupa bene questo tempo per ripassare."
        self.tgmsg(u, out)
        return out


    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        u = None
        compiti = None
        if self.path.startswith("/domani/") or self.path.startswith("/settimana/"):
            u = self.path.split("/")[-1]
            if not u:
                return
            session = self.users[u]
            compiti = session.ask("get_compiti")
        if self.path.startswith("/domani/"):
            self.wfile.write(self.domani(u, session, compiti).encode("utf-8"))
        elif self.path.startswith("/settimana/"):
            self.wfile.write(self.settimana(u, session, compiti).encode("utf-8"))


locale.setlocale(locale.LC_ALL, "it_IT.UTF-8")

if __name__ == "__main__":
    cp = configparser.ConfigParser()
    cp.read("register.conf")
    users = {}
    dests = {}
    for u in cp.sections():
        if cp.has_option(u, "username") and cp.has_option(u, "password"):
            users[u] = connector.login(cp[u]["username"], cp[u]["password"])
            dests[u] = cp[u]["tgid"]
    RegisterAPI.users = users
    RegisterAPI.dests = dests
    RegisterAPI.bot = telegram.Bot(cp["tg"]["key"])
    httpd = HTTPServer(('0', 8000), RegisterAPI)
    httpd.serve_forever()
