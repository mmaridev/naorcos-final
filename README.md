# Naorcos final stuff

This repository contains software used in the final round of
the Italian edition of "NAO Challenge" . Feel free to
download and adapt this software but remember to publish
your modifications, according to the license.
