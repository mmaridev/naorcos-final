<?xml version="1.0" encoding="UTF-8" ?>
<Package name="completone" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="risposta1" src="risposta1/risposta1.dlg" />
        <Dialog name="risposta2" src="risposta2/risposta2.dlg" />
        <Dialog name="risposta3" src="risposta3/risposta3.dlg" />
        <Dialog name="registro" src="registro/registro.dlg" />
        <Dialog name="vela" src="vela/vela.dlg" />
    </Dialogs>
    <Resources>
        <File name="GAME OVER Voice Sound Effect" src="GAME OVER Voice Sound Effect.mp3" />
        <File name="completone finale" src="completone finale.pml" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_iti" src="behavior_1/ExampleDialog/ExampleDialog_iti.top" topicName="ExampleDialog" language="it_IT" />
        <Topic name="risposta1_iti" src="risposta1/risposta1_iti.top" topicName="risposta1" language="it_IT" />
        <Topic name="risposta2_iti" src="risposta2/risposta2_iti.top" topicName="risposta2" language="it_IT" />
        <Topic name="risposta3_iti" src="risposta3/risposta3_iti.top" topicName="risposta3" language="it_IT" />
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="registro_iti" src="registro/registro_iti.top" topicName="registro" language="it_IT" />
        <Topic name="vela_iti" src="vela/vela_iti.top" topicName="vela" language="it_IT" />
    </Topics>
    <IgnoredPaths>
        <Path src="risposta1/risposta1.dlg" />
        <Path src="risposta3/risposta3.dlg" />
        <Path src="risposta2/risposta2.dlg" />
        <Path src="behavior_1/ExampleDialog" />
        <Path src="risposta2/risposta2_iti.top" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog_iti.top" />
        <Path src="behavior_1/behavior.xar" />
        <Path src="risposta1/risposta1_iti.top" />
        <Path src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
        <Path src="risposta3/risposta3_iti.top" />
    </IgnoredPaths>
</Package>
