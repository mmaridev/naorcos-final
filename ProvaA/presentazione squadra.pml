<?xml version="1.0" encoding="UTF-8" ?>
<Package name="presentazione squadra" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="Tu" src="Tu/Tu.dlg" />
        <Dialog name="risposta1&#x0D;" src="risposta1/risposta1.dlg" />
        <Dialog name="risposta2&#x0D;" src="risposta2/risposta2.dlg" />
        <Dialog name="risposta3&#x0D;" src="risposta3/risposta3.dlg" />
    </Dialogs>
    <Resources>
        <File name="Bishop Briggs - River" src="Bishop Briggs - River.mp3" />
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="choice_sentences_light" src="behavior_1/Aldebaran/choice_sentences_light.xml" />
        <File name="GAME OVER Voice Sound Effect" src="GAME OVER Voice Sound Effect.mp3" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="Tu_iti" src="Tu/Tu_iti.top" topicName="Tu" language="it_IT" />
        <Topic name="risposta1_iti" src="risposta1/risposta1_iti.top" topicName="risposta1" language="it_IT" />
        <Topic name="risposta2_iti" src="risposta2/risposta2_iti.top" topicName="risposta2" language="it_IT" />
        <Topic name="risposta3_iti" src="risposta3/risposta3_iti.top" topicName="risposta3" language="it_IT" />
    </Topics>
    <IgnoredPaths />
</Package>
